import cv2
from glob import glob
import numpy as np

from keras.layers.convolutional import Convolution2D
from keras.layers.core import Activation, Dropout
from keras.layers.normalization import BatchNormalization
from keras.models import Sequential, model_from_json
from keras.regularizers import l2

from moviepy.editor import VideoFileClip

import tensorflow as tf

lane_settings = {'name': 'lanes',
                 'presence_weight': 50.0, 'threshold': 0.5,
                 'original_max_x': 1280, 'original_max_y': 720,
                 'crop_min_x': 200, 'crop_max_x': 1080,
                 'crop_min_y': 420, 'crop_max_y': 666,
                 'scale_factor': 2}

car_settings = {'name': 'cars',
                'presence_weight': 50.0, 'threshold': 0.5,
                'original_max_x': 1280, 'original_max_y': 720,
                'crop_min_x': 0, 'crop_max_x': 1280,
                'crop_min_y': 420, 'crop_max_y': 666,
                'scale_factor': 2}

def calibrate_chessboard():
    """Perform calibration using chessboard images"""
    objp = np.zeros((6*9,3), np.float32)
    objp[:,:2] = np.mgrid[0:9,0:6].T.reshape(-1,2)

    calibration_fnames = glob('camera_cal/calibration*.jpg')

    calibration_images = []
    objpoints = []
    imgpoints = []

    for fname in calibration_fnames:
        img = read_image(fname)
        gray = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)
        calibration_images.append(gray)
        ret, corners = cv2.findChessboardCorners(gray, (9,6), None)
        if ret == True:
            objpoints.append(objp)
            imgpoints.append(corners)
    ret, mtx, dist, rvecs, tvecs = cv2.calibrateCamera(objpoints, imgpoints,
                                                     calibration_images[0].shape[::-1], None, None)
    calibration = [mtx,dist]
    return calibration

def undistort(img, calibration):
    return cv2.undistort(img, calibration[0], calibration[1], None, calibration[0])

tf_pos_tanh_offset = tf.constant(0.5)
tf_pos_tanh_scale = tf.constant(0.45)

def read_image(path):
    return cv2.cvtColor(cv2.imread(path), cv2.COLOR_BGR2RGB)

def tanh_zero_to_one(x):
    """Actually [0.05, 0.95] to avoid divide by zero errors"""
    return (tf.tanh(x) * tf_pos_tanh_scale) + tf_pos_tanh_offset

def preprocess_input_image(img,opt):
    img = crop_scale_white_balance(img,opt)
    img = cv2.GaussianBlur(img, (3,3), 0)
    return ((img / 253.0) - 0.5).astype(np.float32)

def scale_white_balance(img,opt):
    img = cv2.resize(img, None, fx=(1.0/opt['scale_factor']), fy=(1.0/opt['scale_factor']),
                   interpolation=cv2.INTER_AREA)
    low = np.amin(img)
    high = np.amax(img)
    img = (((img - low + 1.0) * 252.0 / (high - low)) - 0.5).astype(np.uint8)
    return img

def unscale(img,opt):
    img = cv2.resize(img, None, fx=opt['scale_factor'], fy=opt['scale_factor'])
    if len(img.shape) == 2:
        img = cv2.merge((img,img,img))
    return img


def uncrop_scale(img,opt):
    img = unscale(img,opt)
    img = uncrop(img,opt)
    return img

def crop_scale_white_balance(img,opt):
    img = crop(img,opt)
    img = scale_white_balance(img,opt)
    return img

def crop(img,opt):
    return img[int(opt['crop_min_y']):int(opt['crop_max_y']), int(opt['crop_min_x']):int(opt['crop_max_x'])]

def uncrop(img,opt):
    target_shape = (opt['original_max_y'],opt['original_max_x'], 3)
    frame = np.zeros(target_shape, dtype="uint8")
    frame[opt['crop_min_y']:opt['crop_max_y'], opt['crop_min_x']:opt['crop_max_x'], 0:3] = img
    img = frame
    return img

def read_training_data_paths():
    """Returns {'x': [path1, path2, ...], 'lanes': [path1, path2, ...], 'cars': [path1, path2, ...]}"""
    x = glob('training/*_x.png')
    lanes = glob('training/*_lanes.png')
    cars = glob('training/*_cars.png')
    x.sort()
    lanes.sort()
    cars.sort()
    assert (len(x) == len(lanes)), "x and lanes files don't match"
    assert (len(x) == len(cars)), "x and cars files don't match"
    return {'x': x, 'lanes': lanes, 'cars': cars}


def read_training_data(opt):
    """Returns tuple of input matrix and output matrix (X,y)"""
    paths = read_training_data_paths()
    X = []
    for x in paths['x']:
        X.append(preprocess_input_image(read_image(x), opt))
    Y = []
    for y in paths[opt['name']]:
        Y.append(read_training_file(y,opt))
    return {'x': np.stack(X), 'y': np.stack(Y)}

def read_training_file(fpath,opt):
    """Read (car or lane) annotation file and convert to y format: one channel with
     1 for present or 0 for absent"""
    img = read_image(fpath)
    img = crop_scale_white_balance(img,opt)
    img = cv2.cvtColor(img, cv2.COLOR_RGB2GRAY)
    normalized = np.zeros_like(img)
    normalized[img > 0] = 1
    return np.stack([normalized], axis=-1)


def fit_parabolas_to_lane_centroids(centroids):
    polys = []
    for lane_idx in range(2):
        x_vals = []
        y_vals = []
        for point in centroids[lane_idx]:
            x_vals.append(point[0])
            y_vals.append(point[1])
        min_y = np.amin(y_vals)
        max_y = np.amax(y_vals)
        mid_y = (min_y + max_y) / 2
        weights = []
        for y in y_vals:
            if y > mid_y:
                weights.append(1.0)
            else:
                weights.append(max(0.1, ((y - min_y) * 1.0 / (mid_y - min_y))))
        polys.append(np.polyfit(y_vals, x_vals, 2, w=weights))
    return polys


def annotate_original_image(img, lane_markings_img=None, lane_lines=(None,None), car_img=None):
    if lane_markings_img.any(): # != None:
        markings_pink = np.zeros_like(lane_markings_img)
        markings_gray = cv2.cvtColor(lane_markings_img, cv2.COLOR_RGB2GRAY)
        markings_pink[markings_gray > 100] = np.uint8([255,20,147])
        img = cv2.addWeighted(img, 0.8, markings_pink, 1.0, 0.0)
    if car_img.any(): # != None:
        car_cyan = np.zeros_like(car_img)
        car_gray = cv2.cvtColor(car_img, cv2.COLOR_RGB2GRAY)
        car_cyan[car_gray > 100] = np.uint8([255,0,0])
#         cv2.rectangle(car_cyan, [430,137,1116], [839,503,87], 6)
        img = cv2.addWeighted(img, 0.8, car_cyan, 0.5, 0.0)
        
    if lane_lines[0].any() and lane_lines[1].any(): # != None and lane_lines[1] != None:
        radius = radius_of_lane_lines(lane_lines[0], lane_lines[1])
        offset = offset_from_lane_center(lane_lines[0], lane_lines[1])
        radius_text = "Curvature: Straight"
        if radius and abs(radius) > 100 and abs(radius) < 10000:
            radius_direction = "right"
            if radius > 0:
                radius_direction = "left"
            radius_text = "Lane curvature radius " + str(100 * int(abs(radius) / 100)) + "m to the " + radius_direction
        offset_text = "Vehicle Offset: Center"
        if abs(offset) > 0.1:
            offset_direction = "left"
            if offset > 0:
                offset_direction = "right"
            offset_text = "Vehicle Offset: " + str(int(abs(offset * 10)) / 10.0) + "m to the " + offset_direction
#         cv2.putText(img, radius_text, (80,100), cv2.FONT_HERSHEY_SIMPLEX, 0.7, (255,255,255))
        cv2.putText(img, offset_text, (80,120), cv2.FONT_HERSHEY_SIMPLEX, 0.7, (255,255,255))
        img = draw_lines_on_dash(img, lane_lines)
    return img

def radius_of_lane_lines(left_lane, right_lane):
    #   print('left lane', left_lane)
    #   print('right lane', right_lane)
    if not left_lane.any() or not right_lane.any(): #left_lane == None or right_lane == None:
        return None
    center = (left_lane + right_lane) / 2
    #print("determining radius for " + str(center))
    if abs(center[0]) < 0.000001:
        return None
    radius_pixels = (1 + (2 * center[0] * perspective_max_y + center[1])**2)**1.5 / (-2 * center[0])
    radius_meters = radius_pixels / perspective_pixels_per_meter
    #print("radius is " + str(radius_pixels) + " pixels or " + str(radius_meters) + " meters.")
    return radius_meters

def offset_from_lane_center(left_lane, right_lane):
  
    if not left_lane.any() or not right_lane.any(): #left_lane == None or right_lane == None:
        return 0.0
    center = (left_lane + right_lane) / 2
    lane_offset = center[0] * perspective_max_y**2 + center[1] * perspective_max_y + center[2]
    car_offset = perspective_max_x / 2.0
    #print("Offset... lane: " + str(lane_offset) + " car: " + str(car_offset))
    return (car_offset - lane_offset) / perspective_pixels_per_meter

def draw_lines_on_dash(dash_img, lines):
    perspective_lanes_img = draw_lane_lines(lines)
    dash_lanes_img = perspective_reverse(perspective_lanes_img)
    res = cv2.addWeighted(dash_img, 1, dash_lanes_img, 0.3, 0)
    return res


def draw_lane_lines(lines):
    img = np.zeros((perspective_max_y, perspective_max_x, 3), dtype="uint8")
    points = [[],[]]
    for line_idx in range(2):
        line = lines[line_idx]
        for i in range(31):
            y = int(perspective_max_y * i / 30)
            x = int(line[0] * y**2 + line[1] * y + line[2])
            points[line_idx].append((x,y))
    points[1].reverse()
    cv2.fillPoly(img, np.int_([points[0] + points[1]]), (0,0,255))
    return img

def perspective_reverse(img):
    img = cv2.warpPerspective(img, M_inv, (original_max_x, original_max_y), flags=cv2.INTER_LINEAR)
    return img
